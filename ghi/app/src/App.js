import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelList';
import ModelsForm from './ModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import SalespeopleList from './SalespeopleList';
import SalespeopleForm from './SalespeopleForm';
import CustomersList from './CustomersList';
import CustomersForm from './CustomersForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalespersonHistory from './SalespersonHistory';





function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="manufacturers">
              <Route path="list" element={<ManufacturerList />} />
              <Route path="create" element={<ManufacturerForm />} />
            </Route>
            <Route path="models">
              <Route path="list" element={<ModelsList />} />
              <Route path="create" element={<ModelsForm />} />
          </Route>
            <Route path="automobiles">
              <Route path="list" element={<AutomobileList />} />
              <Route path="create" element={<AutomobileForm />} />
          </Route>
            <Route path="salespeople">
              <Route path="list" element={<SalespeopleList />} />
              <Route path="create" element={<SalespeopleForm />} />
          </Route>
            <Route path="customers">
              <Route path="list" element={<CustomersList />} />
              <Route path="create" element={<CustomersForm />} />
          </Route>
            <Route path="sales">
              <Route path="list" element={<SalesList />} />
              <Route path="create" element={<SalesForm />} />
              <Route path="history" elements={<SalespersonHistory />} />
          </Route>
            <Route path="technicians">
              <Route path="list" element={<TechnicianList />} />
              <Route path="create" element={<TechnicianForm />} />
          </Route>
            <Route path="appointments">
              <Route path="list" element={<AppointmentList />} />
              <Route path="create" element={<AppointmentForm />} />
              <Route path="history" element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;


