import React, {useState, useEffect} from 'react';

function TechnicianList () {

    const [technician, setTechnicians] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
        
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technician)
        }
    }

    useEffect(()=>{
        getData()
    }, [])


    return (
        <>
            <div>
                <h1 className='text-center'>Technicians Available</h1>
            </div>
            <div>
                <table className="table table-success table-striped">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Employee ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        {technician.map(tech => {
                        return (
                            <tr key={tech.id}>
                                <td>{ tech.first_name }</td>
                                <td>{ tech.last_name }</td>
                                <td>{ tech.employee_id }</td>
                            </tr>
                        );
                        })}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default TechnicianList;
