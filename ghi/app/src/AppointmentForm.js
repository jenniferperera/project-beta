import React, {useState, useEffect} from 'react';

function AppointmentForm() {

    const [technician, setTechnician] = useState([])
    
    const [formData, setFormData] = useState({
        date_time: "",
        reason: "",
        vin: "",
        customer: "",
        technician: ""
    });


    const getData = async () => {
        const technicianURL = 'http://localhost:8080/api/technicians/';
        const response = await fetch(technicianURL);
        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technician);
        }
    }
    useEffect(() => {
        getData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const appointmentURL = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            },
        };
        const response = await fetch(appointmentURL, fetchConfig)
        if (response.ok) {
            setFormData({
                date_time: "",
                reason: "",
                vin: "",
                customer: "",
                technician: "",
            });
        }
    }


    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    return (
<div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
        <h1>Create a service appointment</h1>
        <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
                <input value={formData.vin} onChange={handleFormChange} placeholder="VIN Number" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">Automobile VIN...</label>
            </div>
            <div className="form-floating mb-3">
                <input value={formData.customer} onChange={handleFormChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                <label htmlFor="customer">Customer Name...</label>
            </div>
            <div className="form-floating mb-3">
                <input value={formData.date_time} onChange={handleFormChange} placeholder="Date" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                <label htmlFor="date_time">Date and Time...</label>
            </div>
            <div className="mb-3">
                    <select onChange={handleFormChange} value={formData.technician} required name="technician" id="technician" className="form-select">
                        <option value="">Choose a Technician...</option>
                        {technician
                        .sort((a,b) => a.first_name.localeCompare(b.first_name))
                        .map(tech => {
                        return (
                            <option key={tech.id} value={tech.id}>{tech.first_name} {tech.last_name}</option>
                        )
                        })}
                    </select>
            </div>
            <div className="form-floating mb-3">
                <input value={formData.reason} onChange={handleFormChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                <label htmlFor="reason">Reason for Appointment...</label>
            </div>
            <button className="btn btn-success">Create</button>
        </form>
        </div>
    </div>
</div>
    )
}

export default AppointmentForm;
