import React, { useEffect, useState } from 'react';

function SalesForm() {

    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [automobiles, setAutomobiles] = useState([])
    const [salesperson, setSalesperson] = useState('')
    const [customer, setCustomer] = useState('')
    const [automobile, setAutomobile] = useState('')
    const [price, setPrice] = useState(0)


    const handleSalespersonChange = (e) => {
        const value = e.target.value;
        setSalesperson(value)
    }
    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value)
    }
    const handleAutomobileChange = (e) => {
        const value = e.target.value;
        setAutomobile(value)
    }
    const handlePriceChange = (e) => {
        const value = e.target.value;
        setPrice(value)
    }
        const handleSubmit = async (e) => {
            e.preventDefault()
            const data = {
                automobile,
                salesperson_id: salesperson,
                customer_id: customer,
                price
              }

              console.log(data)

              const salesURL = 'http://localhost:8090/api/sales/'
              const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                  'Content-Type': 'application/json',
                }
              }

              const salesResponse = await fetch(salesURL, fetchConfig)
              if (salesResponse.ok) {
                const sale = await salesResponse.json()
                console.log(sale)
                setSalesperson('');
                setCustomer('');
                setAutomobile('');
                setPrice(0);
              }
    }
    const fetchData = async () => {
        const salespeopleURL = 'http://localhost:8090/api/salespeople/';

        const salespeopleResponse = await fetch(salespeopleURL)

        if (salespeopleResponse.ok) {
            const data = await salespeopleResponse.json()
            setSalespeople(data.salespeople)
        }
        const customersURL = 'http://localhost:8090/api/customers/';

        const customersResponse = await fetch(customersURL)

        if (customersResponse.ok) {
            const data = await customersResponse.json()
            setCustomers(data.customers)
        }
        const autosURL = 'http://localhost:8100/api/automobiles/';

        const autosResponse = await fetch(autosURL)

        if (autosResponse.ok) {
            const data = await autosResponse.json()
            setAutomobiles(data.autos)
        }
    }
    useEffect(() => {
        fetchData();
      }, [])
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="mb-3">
                        <div className="mb-3">
                        <label class="form-label">Automobile VIN</label>
                            <select onChange={handleAutomobileChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an automobile VIN...</option>
                                {automobiles.map(automobile => {
                                    return (
                                        <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                                    )
                                })};
                            </select>
                        </div>
                        <label class="form-label">Salesperson</label>
                            <select onChange={handleSalespersonChange} value={salesperson}  required name="salesperson" id="salesperson" className="form-select">
                                <option value="">Choose a salesperson...</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id}  value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                                    )
                                })};
                            </select>
                        </div>
                        <div className="mb-3">
                        <label class="form-label">Customer</label>
                            <select onChange={handleCustomerChange} value={customer}  required name="customer" id="customer" className="form-select">
                                <option value="">Choose a customer...</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                    )
                                })};
                            </select>
                        </div>
                        <label class="form-label">Price</label>
                        <div className="mb-3">
                            <input onChange={handlePriceChange} value={price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )

}

export default SalesForm;



