import React, { useState, useEffect } from 'react';

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    async function loadSalespeople() {
        try {
          const response = await fetch('http://localhost:8090/api/salespeople/');
          if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
          const data = await response.json();
          setSalespeople(data.salespeople);
        } catch (error) {
          console.error(error);
        }
      }

    loadSalespeople();
  }, []);

  return (
    <>
    <h1 className='text-start'>Salespeople</h1>
    <table className="table table-success table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {salespeople.map(salesperson => (
          <tr key={salesperson.id}>
            <td>{salesperson.employee_id}</td>
            <td>{salesperson.first_name}</td>
            <td>{salesperson.last_name}</td>
          </tr>
        ))}
      </tbody>
    </table>
    </>
  );
}

export default SalespeopleList;
