import React, { useState} from 'react';


function SalespeopleForm(){
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [employee_id, setEmployeeId] = useState('')
    // employee_id was employee_Id which was giving me a bad 400 request
    // make sure to match
    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;
        console.log("Data", data)
        const salespeopleUrl = "http://localhost:8090/api/salespeople/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json",
            },
        };
        console.log("salespeopleUrl:", salespeopleUrl);
        console.log("data:", data);
        const response = await fetch(salespeopleUrl, fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();
            console.log(newSalesPerson);
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            window.location.reload()
        }
    };

    const handleFormChangeFirstName = async (event) => {
        const { value } = event.target;
        setFirstName(value);
    }

    const handleFormChangeLastName = async (event) => {
        const { value } = event.target;
        setLastName(value);
    }

    const handleFormChangeEmployeeId = async (event) =>{
        const { value } = event.target;
        setEmployeeId(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
                <h1>Add a Salesperson</h1>
                <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFormChangeFirstName} value ={first_name} placeholder ="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="first name">First name...</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleFormChangeLastName} value={last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="last name">Last name...</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleFormChangeEmployeeId} value={employee_id} placeholder="Employee ID" required type="text" name="employee_Id" id="employee_Id" className="form-control" />
                    <label htmlFor="Employee ID">Employee ID...</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        )
    }
export default SalespeopleForm
