# CarCar

CarCar is an application for managing the aspects of an automobile dealership. It manages the inventory, automobile sales, and automobile services.

Team 26:

* **Jamie Rocha** - Auto Sales
* **Jennifer Perera** - Automobile Services

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<https://gitlab.com/jenniferperera/project-beta>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

![Img](<Screenshot 2023-12-22 182928.png>)

## Design

CarCar is made up of 3 microservices that interact with one another.

- **Inventory**
- **Services**
- **Sales**

### Please view Services Microservice **DIAGRAM** below for visual references:

https://excalidraw.com/#json=-RiYT8NTpsVGGoH3IPNr7,r-XOAtcB2erpDcWpQ0FEGQ

### Please view Sales Microservice **DIAGRAM** below for visual references:

https://excalidraw.com/#json=uTO7xw9MvJwLDp0GrPGJh,oIsUrqF_Ze-IRv5Tjuo-qQ


## Integration

Our Inventory and Sales domains work together with our Services domains to make everything here at **CarCar** possible.

How this all starts is at our inventory domain. We keep a record of automobiles on our lot that are available to buy. Our sales and service microservices obtain information from the inventory domain, using a **poller**, which talks to the inventory domain to keep track of which vehicles we have in our inventory so that the service and sales team always has up-to-date information.


## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Manufacturers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/


JSON body to send data:

Create and Update a manufacturer (SEND THIS JSON BODY):
- You cannot make two manufacturers with the same name
```
{
  "name": "Chrysler"
}
```
The return value of creating, viewing, updating a **SINGLE** manufacturer:
```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Chrysler"
}
```
Getting a **list of manufacturers** return value:
```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Mercedes-Benz"
    },
    {
      "href": "/api/manufacturers/2/",
      "id": 2,
      "name": "Chrysler"
    }
  ]
}
```

### Vehicle Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

**Create** and **Update** a vehicle model (SEND THIS JSON BODY):
```
{
  "name": "Wagon",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or picture URL:
```
{
  "name": "Wagon",
  "picture_url": "image.yourpictureurl.com"
}
```
Return value of creating or updating a vehicle model:
- This returns the manufacturer's information as well
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Wagon",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Mercedes-Benz"
  }
}
```
Getting a **List of Vehicle Models** Return Value:
```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Wagon",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Mercedes-Benz"
      }
    },
    {
      "href": "/api/models/2/",
      "id": 2,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/2/",
        "id": 2,
        "name": "Chrysler"
      }
    }
  ]
}
```

### Automobiles:
- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/


Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin. Also, if you don't specify wether the car is sold(true), it will default to not sold(false). 
```
{
  "color": "red",
  "year": 2012,
  "vin": "777GHTNSMYYDHY",
  "model_id": 1,
  "sold": true
}
```
Return Value of Creating an Automobile:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777GHTNSMYYDHY",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	},
    "sold": true
}
```
To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

Return Value:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "green",
  "year": 2011,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/2/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}
```
You can update the color, year and/or sold status of an automobile. To do this, send the request by its VIN; example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/
(SEND THIS JSON BODY):
```
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```
Getting a **list of Automobiles** Return Value:
```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "red",
      "year": 2012,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": true
    }
  ]
}
```

# Sales Microservice

The backend architecture holds 4 models: named 'AutomobileVO', 'Customer', 'Salesperson' and 'Sale'. In this case, the Sale model interfaces with the other remaining 3 models. It references them to output a completed sale structure that provides the details of a completed sale. Sale has a property, and 3 foreignkeys tied to it. It's property is price, and the three Foreign Keys are automobile, salesperson, and customer. We require 3 foreign keys because none of the models are integrated with each other. Sale uses the automobile foreign key to reference the AutomobileVO, grabbing the sold and vin properties handy, in order to execute our instructions, which in this case, for vin is to: show, and for sold, is to: update the status to true (meaning it's been sold). Sale uses the salesperson foreign key by referencing the id given, and using that variable to look at the salesperson model, and with this id, it grabs the salesperson attached to that ID and adds it as the designated salesperson for the sale. The ID was grabbed using salesperson_id; a feature built by foreign key. Among all the content on this sale, customer is the last key to be integrated into the Sale model, it grabs the customer_id which was created when the foreign key was implemented. Sources through to the model, and brings that information back to take its rightful place on the customer field.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data.

The reason we integrated these two microservices is because I need information from the Inventory microservice in order to show a full sale, we need to have VIN's and sold statuses readily available. We cannot try to sell a customer a vehicle if we don't know if we even have that vehicle available(not sold) or even in stock!(VIN)


## Accessing Endpoints to Send and View Data - Access through Insomnia:

### Customers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Show a specific customer | GET | http://localhost:8090/api/customers/id/
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/id/

To create a Customer (SEND THIS JSON BODY):
```
{
	"first_name": "SirHonks",
	"last_name": "ALot",
	"address": "1800 Nunya Avenue",
	"phone_number": 8134562587
}
```
Return Value of Creating a Customer:
```
{
	"first_name": "SirHonks",
	"last_name": "ALot",
	"address": "1800 Nunya Avenue",
	"phone_number": 8134562587,
	"id: "1"
}
```
Return value of Listing all Customers:
```
{
	"customers": [
		{
			"first_name": "SirHonks",
			"last_name": "ALot",
			"address": "1800 Nunya Avenue",
			"phone_number": 8134562587,
			"id: "1"
		},
		{
			"first_name": "Captain",
			"last_name": "Carpool",
			"address": "1600 Nunya Avenue",
			"phone_number": 5789456356,
			"id: "2"
		}
	]
}
```
### Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/id/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/


To create a salesperson (SEND THIS JSON BODY):
```
{
	"first_name": "Billy",
	"last_name": "Dun",
	"employee_id": "1"
}
```
Return Value of creating a salesperson:
```
{
	"id": 1,
	"first_name": "Billy",
	"last_name": "Dun",
	"employee_id": "1"
}
```
List all salespeople Return Value:
```
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "Billy",
			"last_name": "Dun",
			"employee_id": "1"
		},
		{
			"id": 2,
			"first_name": "Ken",
			"last_name": "Tron",
			"employee_id": "2"
		}
	]
}
```
### Sales:
- the id value to show a salesperson's sales is the **"id" value tied to a salesperson.**

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all sales | GET | http://localhost:8090/api/sales/
| Sale Details | GET | http://localhost:8090/api/sales/id/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Delete a sale | DELETE | http://localhost:8090/api/sales/id/

List all Sales Return Value:
```
{
	"sales": [
		{
			"id": 1,
			"price": 23000,
			"automobile": "712JSUWMQL2KA3FUE",
			"customer": 1,
			"salesperson": "1"
		},
		{
			"id": 2,
			"price": 23000,
			"automobile": "712JSUWMQL2KA3FUE",
			"customer": 2,
			"salesperson": "1"
		}
	]
}
```
Create a New Sale (SEND THIS JSON BODY):
```
{
	"automobile": "712JSUWMQL2KA3FUE",
	"salesperson": 1,
	"customer": 1,
	"price": 23000
}
```
Return Value of Creating a New Sale:
```
{
	"id": 1,
	"automobile": {
		"vin": "712JSUWMQL2KA3FUE",
		"sold": true
	},
	"price": 23000,
	"salesperson": {
		"id": 1,
		"first_name": "Billy",
		"last_name": "Dun",
		"employee_id": "1"
	},
	"customer": {
		"id": 1,
		"first_name": "SirHonks",
		"last_name": "ALot",
		"address": "1800 Nunya Avenue",
		"phone_number": 8134562587
	}
}
```


# Services Microservice 

On the backend of this application, the services microservice has 3 models: *AutomobileVO, Technician, and Appointments*. Appointments is the model that interacts with the other 2 models.

The **AutomobileVO** is a *value object* that gets data about the automobiles in the inventory using a poller. The services poller automatically polls the inventory microservice for data, constantly getting the updated data.

The reason for integration between these two microservices is when recording a new appointment, you'll need to choose which car is being assigned to that appointment and that information lives inside of the inventory microservice's model named Automobile.

This area is going to be broken down into the various API endpoints for services along with the format needed to send data to each component.
The basics of service are as follows:

1. Our friendly technician staff
2. Service Appointments


### Technicians :

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Technician detail | GET | http://localhost:8080/api/technicians/<int:pk>/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/


**LIST TECHNICIANS**: Following this endpoint will give you a list of all technicians that are currently employed.
Since this is a GET request, you do not need to provide any data.

```
Example:
{
	"technician": [
		{
        "first_name": "Donald",
        "last_name": "Trump",
        "employee_number": 1,
        "id": 1
		},
    ]
}
```

**TECHNICIAN DETAIL**: This is a GET request as well, so no data needs to be provided here either. When you list technicians, you will
see that they are assigned a value of "id". This is the value that will replace "<int:pk>. For example, if you wanted to see the technician
details related to our technician "Donald", you would input the following address: http://localhost:8080/api/technicians/1/

This would then lead to this:

```
{
	"first_name": "Donald",
	"last_name": "Trump",
	"employee_number": 1,
	"id": 1
}
```

This how our technician detail is displayed. If you want to change the technician, just change the value at the end to match the "id" of the technician you want to display.

**CREATE TECHNICIAN** - To create a technician, you would use the following format to input the data and you would just submit this as a POST request.

```
{
	"first_name": "Julia",
	"last_name": "Roberts",
	"employee_id": 2,
	"id": 2
}
```
In this example, we just changed the "name" field from "Donald" to "Julia". We also assigned her the "employee_id" value of "2" instead of "1".
Once we have the data into your request, we just hit "Send" and it will create the technician "Julia". To verify that it worked, just select follow the **"LIST TECHNICIAN"** step from above to show all technicians.
With any luck, both Donald and Julia will be there.
Here is what you should see if you select "LIST TECHNICIAN" after you **"CREATE TECHNICIAN"** with Julia added in:

```
{
    "technician": [
        {
            "first_name": "Donald",
            "last_name": "Trump",
            "employee_number": 1,
            "id": 1
        },
        {
            "first_name": "Julia",
            "last_name": "Roberts",
            "employee_id": 2,
            "id": 2
        }
    ]
}
```

**DELETE TECHNICIAN** - To do this, just change the request type to "DELETE". You also need to pull the "id" value just like you did in "TECHNICIAN DETAIL" to make sure you delete the correct one.

The "id" field is AUTOMATICALLY generated by the program so you don't have to input that information. Just follow the steps in CREATE TECHNICIAN and the "id" field will be populated for you.

If you get an error, make sure your server is running and that you are feeding it in the data that it is requesting.
If you feed in the following, it will not work because we didn't specify any "favorite_auto" field so it does not know what to do with "Muscle Cars".

```
{
  "first_name": "Dana",
  "last_name": "Wynd",
  "favorite_auto": "Muscle Cars"
}
```

### Service Appointments:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Service appointment detail | GET | http://localhost:8080/api/appointments/<int:id>
| Service appointment history | GET | http://localhost:8080/api/appointments/
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Update appointment "Cancelled" | PUT | http://localhost:8080/api/appointments/<int:id>/cancelled/
| Update appointment "Finished" | PUT | http://localhost:8080/api/appointments/<int:id>/finished/
| Delete service appointment | DELETE | http://localhost:8080/api/appointments/<int:id>



**LIST SERVICE APPOINTMENT**: This will return a list of all current service appointments.
This is the format that will be displayed:

```
{
	"appointments": [
		{
			"date_time": "2024-01-17T10:00:00+00:00",
			"reason": "Rear Lights Repair",
			"status": "Created",
			"vin": "WBAVB33576AZ30123",
			"customer": "Sara Jules",
			"technician": {
				"first_name": "Milo",
				"last_name": "McTri",
				"employee_id": 4,
				"id": 1
			},
			"sold": false,
			"id": 41
		}
	]
}
```

**SERVICE APPOINTMENT DETAIL**: This will return the detail of each specific service appointment. Make sure to input "id" at the end of url like so: (http://localhost:8080/api/appointments/41/) 

```
Expected output:

{
	"date_time": "2024-01-17T10:00:00+00:00",
	"reason": "Rear Lights Repair",
	"status": "Created",
	"vin": "WBAVB33576AZ30123",
	"customer": "Sara Jules",
	"technician": {
		"first_name": "Milo",
		"last_name": "McTri",
		"employee_id": 4,
		"id": 4
	},
	"sold": false,
	"id": 41
}
```

**CREATE SERVICE APPOINTMENT** - This will create a service appointment with these data input. Must follow this format.

```
{
	"date_time": "2023-12-19 ", 
	"reason": "Brake Fuild",
	"vin": "5YJXCDE25KF123456",
	"customer": "Tim McDonald",
	"technician": 2
}

```
**DELETE SERVICE APPOINTMENT** - To delete an appointment, you will send a *DELETE* request with 'http://localhost:8080/api/appointment/int:id/' . We will receive a confirmation message saying that
the service appointment was deleted like so:

```
Expected output:
{
	"message": "Appointment was deleted"
}
```

And if clicked again, you will get a 404 Page not Found Error because appointment does not exist. 

**UPDATE APPOINTMENT STATUS** 

Use a **PUT** request to set your appointment status from "Created" to **'Finished'** with url:
http://localhost:8080/api/appointments/int:id/finished/

```
Expected output:
{
	"date_time": "2023-12-29T13:37:00+00:00",
	"reason": "Annual Check Up",
	"status": "Finished",
	"vin": "1HGCM82633A301234",
	"customer": "John Smith",
	"technician": {
		"first_name": "Milo",
		"last_name": "McTri",
		"employee_id": 4,
		"id": 4
	},
	"sold": true,
	"id": 49
}
```

Use a **PUT** request to set your appointment status from "Created" to **'Cancelled'** with url:
http://localhost:8080/api/appointments/int:id/cancelled/

```
Expected output:
{
	"date_time": "2023-12-29T13:37:00+00:00",
	"reason": "Annual Check Up",
	"status": "Cancelled",
	"vin": "1HGCM82633A301234",
	"customer": "John Smith",
	"technician": {
		"first_name": "Milo",
		"last_name": "McTri",
		"employee_id": 4,
		"id": 4
	},
	"sold": true,
	"id": 49
}
```
