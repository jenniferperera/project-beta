from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AutomobileVO, Appointment, Technician
from django.views.decorators.http import require_http_methods
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["sold", "import_href", "vin", "id"]

    def get_extra_data(self, o):
        return {"vin": o.vin}


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time", 
        "reason", 
        "status", 
        "vin", 
        "customer", 
        "technician",
        "sold",
        "id"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder,
            safe=False   
    )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create the technician"},
                status=400
            )


@require_http_methods(["GET", "DELETE"])
def api_show_technicians(request, pk):
    if request.method=="GET":
        technician = get_object_or_404(Technician, id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        technicians = get_object_or_404(Technician, id=pk)
        technicians.delete()
        return JsonResponse(
            {"message": "Technician deleted"}
            )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            if AutomobileVO.objects.filter(vin=content["vin"]).exists():
                content["sold"]= True
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
            content["status"] = "Created"
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
                )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create the appointment."},
                status=400
            )


@require_http_methods(["GET", "DELETE"])
def api_show_appointments(request, pk):
    if request.method=="GET":
        appointment = get_object_or_404(Appointment, id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    else:
        appointment = get_object_or_404(Appointment, id=pk)
        appointment.delete()
        return JsonResponse(
            {"message": "Appointment was deleted"}
            )


@require_http_methods(["PUT"])
def  api_finished_appointments(request, pk):
    appointment = get_object_or_404(Appointment, id=pk)
    appointment.finished()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def  api_cancelled_appointments(request, pk):
    appointment = get_object_or_404(Appointment, id=pk)
    appointment.cancelled()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )
