from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=100, unique=True)
    sold = models.BooleanField(default=False)

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.PositiveSmallIntegerField()


class Appointment(models.Model):
    date_time = models.DateTimeField(blank=True)
    reason = models.TextField()
    status = models.CharField(max_length=10, blank=True)
    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        blank=True,
        on_delete=models.PROTECT,
    )
    sold = models.BooleanField(default=False, blank=True)


    def finished(self):
        self.status = "Finished"
        self.save()

    def cancelled(self):
        self.status = "Cancelled"
        self.save()    

    def __str__(self):
        return self.customer
    
    def get_api_url(self):
        return reverse("api_list_appointments", kwargs={"pk": self.pk})
