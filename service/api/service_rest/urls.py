from django.contrib import admin
from django.urls import path
from .views import api_list_technicians, api_list_appointments, api_show_technicians, api_show_appointments
from .views import api_finished_appointments, api_cancelled_appointments

urlpatterns = [
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_show_appointments, name="api_show_appointments"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_show_technicians, name="api_show_technicians"),
    path(
        "appointments/<int:pk>/finished/",
        api_finished_appointments,
        name="api_finished_appointments",
    ),
    path(
        "appointments/<int:pk>/cancelled/",
        api_cancelled_appointments,
        name="api_cancelled_appointments",
    )]
