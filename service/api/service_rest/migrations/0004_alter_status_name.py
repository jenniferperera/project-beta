# Generated by Django 4.0.3 on 2023-12-19 20:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0003_status_alter_appointment_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='name',
            field=models.CharField(default='pending', max_length=10, unique=True),
        ),
    ]
