from .encoders import SalespersonEncoder, CustomerEncoder, SaleEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
import json


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Invalid Information"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_salesperson(request, pk):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson matching Id does not exist"},
                status = 404
                )
    else:
        try:
            salespeople = Salesperson.objects.get(id=pk)
            salespeople.delete()
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        try:
            customer = Customer.objects.all()
            return JsonResponse(
                {"customers": customer},
                encoder=CustomerEncoder,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder = CustomerEncoder,
                safe = False
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer matching Id does not exist"},
                status = 404
            )
    else:
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "DELETE"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder = SaleEncoder,
                safe = False
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale matching Id does not exist"},
                status = 404
            )
    else:
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
        

@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        sales_data = [
            {
                "id": sale.id,
                "price": sale.price,
                "automobile": sale.automobile.vin,
                "customer": sale.customer.id,
                "salesperson": sale.salesperson.employee_id,
            }
            for sale in sales
        ]
        return JsonResponse({"sales": sales_data}, encoder=SaleEncoder)
    elif request.method == "POST":
        content = json.loads(request.body)
        automobile_vin = content["automobile"]
        customer_id = content["customer"]
        salesperson_id = content["salesperson"]
        try:
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content['automobile']=automobile
            automobile.sold = True
            automobile.save()
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": f"Automobile with VIN: {automobile_vin} does not exist"}, status=404)
        try:
            salesperson = Salesperson.objects.get(pk=salesperson_id)
            content["salesperson"]=salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": f"Salesperson with ID: {salesperson_id} does not exist"}, status=404)
        try:
            customer = Customer.objects.get(pk=customer_id)
            content['customer']=customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": f"Customer with ID: {customer_id} does not exist"}, status=404)
        
        sale = Sale.objects.create(**content)

        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
        )


@require_http_methods(["GET"])
def salesperson_sales(request, employee_id):
    salesperson = get_object_or_404(Salesperson, employee_id=employee_id)
    sales = Sale.objects.filter(salesperson=salesperson)
    sales_list = [SaleEncoder().default(sale) for sale in sales]
    return JsonResponse({"sales": sales_list}, safe=False)
